/**
 * Module dependencies.
 */
import * as bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import * as errorHandler from 'errorhandler';
import * as express from 'express';
import * as mongoose from 'mongoose';
import * as logger from 'morgan';
import * as path from 'path';

import taskRouter from './routes/task.route';
import todoRouter from './routes/todo.route';
import userRouter from './routes/user.route';

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.config({ path: '.env' });

/**
 * Routes
 */
class App {
  // ref to Express instance
  public express: express.Application;
  // private readonly MongoStore = mongo(session);

  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
    this.launchConf();
  }
  private middleware(): void {
    this.express.set('port', process.env.PORT || 3000);
    this.express.set('views', path.join(__dirname, '../', 'public', 'views'));
    this.express.set('view engine', 'pug');
    this.express.use(logger('dev'));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: true }));
    this.express.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));
  }
  /**
   * Primary app routes.
   */
  private routes(): void {
    this.express.use('/tasks', taskRouter);
    this.express.use('/todos', todoRouter);
    this.express.use('/users', userRouter);
  }

  private launchConf() {
    // mongoose.Promise = global.Promise;
    mongoose.connect(process.env.MONGODB_URI);

    mongoose.connection.on('error', () => {
      // tslint:disable-next-line:no-console
      console.log('MongoDB connection error. Please make sure MongoDB is running.');
      process.exit();
    });

    this.express.use(errorHandler());

    /**
     * Start Express server.
     */
    this.express.listen(this.express.get('port'), () => {
      // tslint:disable-next-line:no-console
      console.log('  App is running at http://localhost:%d \
      in %s mode', this.express.get('port'), this.express.get('env'));
      // tslint:disable-next-line:no-console
      console.log('  Press CTRL-C to stop\n');
    });
  }
}

export default new App().express;
