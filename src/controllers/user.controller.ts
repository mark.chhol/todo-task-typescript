import * as bcrypt from 'bcrypt';
import { Request, Response } from 'express';
import { configure, validate } from 'indicative/validator';
import Task from '../schema/task.schema';
import Todo from '../schema/todo.schema';
import User from '../schema/user.schema';

const saltRounds = 10;

/* GET all users listing. */
// GET /users?limit=10&offset=1
// GET /users?search=Something
// GET /users?sortBy=firstname:asc or desc
// GET /users?sortBy=lastname:asc or desc
// GET /users?sortBy=createdAt:desc
export let getUsers = async (req: Request, res: Response) => {
  try {
    const { limit = 10, offset = 0, search = '', sortBy } = req.query;
    let sort = {};
    if (sortBy) {
      const parts = sortBy.split(':');
      const part: string = parts[0];
      sort = { [part]: parts[1] };
      console.log(sort); // { createdAt: 'desc' }
    }
    const user = await User.find({
      firstName: { $regex: search, $options: 'i' },
      status: 'active'
    })
      .limit(+limit)
      .skip(+offset)
      .sort(sort);
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json(error);
  }
};

export let createUser = async (req: Request, res: Response) => {
  try {
    const rules = {
      email: 'required|email',
      password: 'required|min:4',
      firstName: 'required',
      lastName: 'required'
    };
    const messages = {
      required: (field: string) => `${field} is required`,
      'email.email': 'Please enter a valid email address',
      'password.min': 'Password is too short'
    };
    await validate(req.body, rules, messages);

    const { password, firstName, lastName, email } = req.body;
    const isExistingUser = await User.findOne({ email });
    const hash = bcrypt.hashSync(password, saltRounds);
    if (isExistingUser) return res.status(400).json('This email already existed');
    const user = await new User({ firstName, lastName, email, password: hash });
    user.save();
    res.status(201).json('Successful Create User');
  } catch (error) {
    res.status(500).json(error);
  }
};
export let loginUser = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) return res.status(404).json('Email not found');
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) return res.status(400).json('Incorrect password');
    return res.status(200).json('Successful sign in');
  } catch (error) {
    res.status(400).json(error);
  }
};
export let getUser = async (req: Request, res: Response) => {
  try {
    const user = await User.findById(req.params.userId);
    if (!user) return res.status(404).json('User not found');
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json(error);
  }
};
export let updateUser = async (req: Request, res: Response) => {
  try {
    emptyOrSpace(req, res);
    const rules = {
      email: 'email',
      password: 'min:4',
      firstName: 'string | min:2',
      lastName: 'string | min:2'
    };
    const messages = {
      'email.email': 'Please enter a valid email address',
      'password.min': 'Password is too short'
    };
    await validate(req.body, rules, messages, { removeAdditional: true });

    let { password } = req.body;
    if (password) {
      const hash = bcrypt.hashSync(password, saltRounds);
      password = hash;
    }
    const updateObject = req.body; // get password too
    if (updateObject.password) {
      updateObject.password = password;
    }
    const user = await User.findOneAndUpdate(req.params.userId, updateObject);
    if (!user) return res.status(404).json('Update Error');
    res.status(200).json('Update Success');
  } catch (error) {
    res.status(500).json(error);
  }
};
export let deleteUser = async (req: Request, res: Response) => {
  try {
    const user = await User.findById(req.params.userId);
    if (!user) return res.status(404).json('User not found');
    user.status = 'delete';
    await user.save();
    res.status(200).json('Successful delete user');
  } catch (error) {
    res.status(500).json(error);
  }
};
export let findTodoByUserId = async (req: Request, res: Response) => {
  try {
    const { userId } = req.params;
    const user = await User.findById(userId);
    if (!user) return res.status(404).json('User not found');
    const todos = await Todo.find({
      todoOwner: user._id,
      status: 'active'
    })
      .populate('_tasks')
      .then(todos => {
        res.status(200).json(todos);
      });
    res.status(200).json(todos);
  } catch (error) {
    res.status(404).json(error);
  }
};
export let createTodo = async (req: Request, res: Response) => {
  try {
    const rules = {
      description: 'required|string|min:4'
    };
    const messages = {
      required: (field: string) => `${field} is required`,
      'description.string': 'Please enter a valid description in text'
    };
    await validate(req.body, rules, messages, { removeAdditional: true });

    const { userId: userId } = req.params;
    const user = await User.findById(userId);
    if (!user) return res.status(404).json('User not found');

    const todo = new Todo({
      ...req.body, // spread convert array of req.body to obj
      todoOwner: userId
    });
    await todo.save();
    if (!todo) return res.status(400).json('Unable to create todo');
    res.status(201).json('Create Todo Successful');
  } catch (error) {
    res.status(400).json(error);
  }
};
export let updateTodo = async (req: Request, res: Response) => {
  try {
    emptyOrSpace(req, res);
    const rules = {
      description: 'string|min:4'
    };
    const messages = {
      'description.string': 'Please enter a valid description in text'
    };
    await validate(req.body, rules, messages, { removeAdditional: true });

    const updateObject = req.body;
    const { todoId, userId } = req.params;
    const todo = await Todo.findOneAndUpdate({ _id: todoId, todoOwner: userId }, updateObject);
    if (!todo) return res.status(404).json('User/Todo not found');
    res.status(200).json('Successful Updated todo');
  } catch (error) {
    res.status(400).json(error);
  }
};
export let deleteTodo = async (req: Request, res: Response) => {
  try {
    const { todoId, userId } = req.body;
    const todo = await Todo.findOne({
      _id: todoId,
      todoOwner: userId
    });
    if (!todo) return res.status(404).json('Todo not found');
    todo.status = 'delete';
    await todo.save();
    res.json(todo);
  } catch (error) {
    res.status(500).json(error);
  }
};

export let createTask = async (req: Request, res: Response) => {
  try {
    const rules = {
      taskName: 'required|string|min:4',
      dueDate: 'date'
    };
    const messages = {
      required: (field: string) => `${field} is required`,
      'taskName.string': 'Please enter a valid Task name',
      'dueDate.date': 'Please enter date format for example: 2019-08-01'
    };
    await validate(req.body, rules, messages, { removeAdditional: true });

    const { userId, todoId } = req.params;
    const todo = await Todo.findOne({
      _id: todoId,
      todoOwner: userId
    });
    console.log(req.body);
    if (!todo) return res.status(404).json('Todo not found');
    const task = new Task({ ...req.body, todoId });

    const taskCreated = await task.save();
    if(!taskCreated) return res.status(404).json('Unable to create task');
    const addTaskToTodo = await Todo.findByIdAndUpdate(todoId, { $push: {_tasks: taskCreated._id}}, { new: true});
    if(!addTaskToTodo) return res.status(404).json('Unable to add task to Todo');
    res.status(200).json({addTaskToTodo});

    res.status(201).json('Success created Task');
  } catch (error) {
    res.status(400).json(error);
  }
};

export let getTasks = async (req: Request, res: Response) => {
  try {
    const { todoId, userId } = req.params;
    const user = await User.findById(userId);
    if (!user) return res.status(404).json('User not found');
    // TODO transform to res outside todo
    const todos = await Todo.find({
      _id: todoId,
      todoOwner: user._id,
      status: 'active'
    })
      .populate('_tasks')
      .then(todos => {
        res.status(200).json(todos);
      });
  } catch (error) {
    res.status(400).json(error);
  }
};
export let getTask = async (req: Request, res: Response) => {
  try {
    const task = await findTask(req, res);
    res.status(200).json(task);
  } catch (error) {
    res.status(400).json(error);
  }
};
export let updateTask = async (req: Request, res: Response) => {
  try {
    emptyOrSpace(req, res);
    const rules = {
      taskName: 'string|min:4',
      dueDate: 'date'
    };
    const messages = {
      required: (field: string) => `${field} is required`,
      'taskName.string': 'Please enter a valid Task name',
      'dueDate.date': 'Please enter date format for example: 2019-08-01'
    };
    await validate(req.body, rules, messages, { removeAdditional: true });

    const updateObject = req.body;
    await findTask(req, res);
    const task = await Task.findByIdAndUpdate(req.params.taskId, updateObject);
    task.save();
    res.status(200).json('Successful Updated task');
  } catch (error) {
    res.status(400).json(error);
  }
};
export let deleteTask = async (req: Request, res: Response) => {
  try {
    await findTask(req, res);
    const task = await Task.findByIdAndUpdate(req.params.taskId, { status: 'delete' });
    task.save();
    res.status(200).json('Deleted Task successfully');
  } catch (error) {
    res.status(400).json(error);
  }
};
const findTask = async (req: Request, res: Response) => {
  try {
    const { userId, todoId, taskId } = req.params;
    const user = await User.findById(userId);
    if (!user) return res.status(404).json('User not found');
    const todos = await Todo.find({
      _id: todoId,
      todoOwner: user._id,
      status: 'active'
    });
    if (!todos) return res.status(404).json('Todo not found');
    const task = await Task.find({ _id: taskId, status: 'active' }, { __v: 0, updatedAt: 0, createdAt: 0 });
    if (!task) return res.status(404).json('Task not found');
    return task;
  } catch (error) {
    return res.status(400).json(error);
  }
};

export let emptyOrSpace = async (req: Request, res: Response) => {
  try {
    for (const key in req.body) {
      if (req.body[key] === '' || req.body[key] === null || req.body[key].match(/^ *$/) !== null) {
        return res.status(400).json(key + ' is required ');
      }
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};
