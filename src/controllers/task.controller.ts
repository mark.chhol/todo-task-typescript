import { Request, Response } from 'express';
import Task from '../schema/task.schema';

export let getTasks = async (req: Request, res: Response) => {
  try {
    const { offset = 0, limit = 10, match = 'false', search = '', sortBy } = req.query;
    let sort = {};
    if (sortBy) {
      const parts = sortBy.split(':');
      const part: string = parts[0];
      sort = { [part]: parts[1] };
    }
    const tasks = await Task.find(
      {
        taskName: { $regex: search, $options: 'i' },
        status: 'active'
      },
      { __v: 0, updatedAt: 0, createdAt: 0 }
    )
      .populate({
        path: 'todoId',
        populate: { path: 'todoOwner' }
      })
      .limit(+limit)
      .skip(+offset)
      .sort(sort);

    res.status(200).json(tasks);
  } catch (error) {
    res.status(400).json(error);
  }
};
