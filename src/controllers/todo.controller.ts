import { Request, Response } from 'express';
import Todo from '../schema/todo.schema';

// Get all todo from all user
// GET /todos?limit=10&offset=1
// GET /todos?sortBy=description:asc or desc
// GET /todos?sortBy=createdAt:desc or updatedAt
// GET /todos?search=this is my search
export let getTodos = async (req: Request, res: Response) => {
  try {
    console.log('Get todos');
    const { offset = 0, limit = 10, search = '', sortBy } = req.query;
    let sort = {};
    if (sortBy) {
      const parts = sortBy.split(':');
      const part: string = parts[0];
      sort = { [part]: parts[1] };
    }
    const todos = await Todo.find(
      {
        description: { $regex: search, $options: 'i' },
        status: 'active'
      },
      { __v: 0, updatedAt: 0, createdAt: 0 }
    )
      .populate('_tasks', 'taskName status', 'Task')
      .limit(+limit)
      .skip(+offset)
      .sort(sort);
    res.status(200).json(todos);
  } catch (error) {
    res.status(500).json(error);
  }
};
