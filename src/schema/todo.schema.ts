import * as mongoose from 'mongoose';

export type TodoModel = mongoose.Document & {
  description: string;
  status: string;
  todoOwner: { type: mongoose.Schema.Types.ObjectId; ref: 'User' };
  _tasks: [{ type: mongoose.Schema.Types.ObjectId; ref: 'Task' }];
};

const todoSchema = new mongoose.Schema(
  {
    description: { type: String, required: true },
    status: { type: String, default: 'active' },
    todoOwner: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' }, // User should change to users
    _tasks: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Task' }]
  },
  { timestamps: true }
);

todoSchema.virtual('Task', {
  foreignField: 'todoId',
  localField: '_id',
  ref: 'Task'
});

const Todo = mongoose.model<TodoModel>('Todo', todoSchema, 'todos');
export default Todo;
