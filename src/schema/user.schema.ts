import * as bcrypt from 'bcrypt';
import * as mongoose from 'mongoose';

export type UserModel = mongoose.Document & {
  email: string;
  firstName: string;
  lastName: string;
  status: string;
  password: string;
};

const userSchema = new mongoose.Schema(
  {
    email: { required: true, type: String },
    firstName: { required: true, type: String },
    lastName: { required: true, type: String },
    status: { type: String, default: 'active' },
    password: { type: String }
  },
  { timestamps: true }
);

userSchema.virtual('Todo', {
  foreignField: 'todoOwner',
  localField: '_id',
  ref: 'Todo'
});

userSchema.statics.findByCredentials = async (email: string, password: string) => {
  const user = await User.findOne({ email });
  if (!user) {
    throw new Error('Unable to login');
  }
  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    throw new Error('Unable to login');
  }
  return user;
};

// export const User: UserType = mongoose.model<UserType>('User', userSchema);
const User = mongoose.model<UserModel>('User', userSchema, 'users');

export default User;
