import * as mongoose from 'mongoose';

export type TaskModel = mongoose.Document & {
  taskName: string;
  dueDate: Date;
  status: string;
  todoId: { type: mongoose.Schema.Types.ObjectId; ref: 'Todo' };
};

const taskSchema = new mongoose.Schema(
  {
    taskName: { type: String, required: true },
    dueDate: { type: Date },
    status: { type: String, default: 'active' },
    todoId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Todo' }
  },
  { timestamps: true }
);

taskSchema.virtual('todos', {
  foreignField: '_tasks',
  localField: '_id',
  ref: 'todos'
});

const Task = mongoose.model<TaskModel>('Task', taskSchema, 'tasks');
export default Task;
