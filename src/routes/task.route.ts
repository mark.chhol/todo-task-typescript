import { NextFunction, Request, Response, Router } from 'express';
import * as taskController from '../controllers/task.controller';

class Task {
  public router: Router;
  public constructor() {
    this.router = Router();
    this.init();
  }
  private init() {
    // Get task with populate of todo and populate of todoOwner(userId)
    this.router.get('/', taskController.getTasks);
  }
}

const taskRoutes = new Task();
export default taskRoutes.router;
