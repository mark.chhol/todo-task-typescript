import { NextFunction, Request, Response, Router } from 'express';
import * as todoController from '../controllers/todo.controller';
class Todo {
  public router: Router;
  public constructor() {
    this.router = Router();
    this.init();
  }
  private init() {
    // Get todo with populate of tasks by select tasks titleName and status
    this.router.get('/', todoController.getTodos);
  }
}

const todoRoutes = new Todo();
export default todoRoutes.router;
