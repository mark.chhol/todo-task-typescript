import { NextFunction, Request, Response, Router } from 'express';
import * as userController from '../controllers/user.controller';

class User {
  public router: Router;
  public constructor() {
    this.router = Router();
    this.init();
  }
  private init() {
    this.router.get('/', userController.getUsers);
    this.router.post('/', userController.createUser);
    this.router.post('/login', userController.loginUser);
    this.router.get('/:userId', userController.getUser);
    this.router.put('/:userId', userController.updateUser);
    this.router.delete('/:userId', userController.deleteUser);
    this.router.get('/:userId/todos', userController.findTodoByUserId); // will also list all the tasks with each todo
    this.router.post('/:userId/todos', userController.createTodo);
    this.router.put('/:userId/todos/:todoId', userController.updateTodo);
    this.router.delete('/:userId/todos/:todoId', userController.deleteTodo);
    this.router.post('/:userId/todos/:todoId/tasks', userController.createTask);
    this.router.get('/:userId/todos/:todoId/tasks', userController.getTasks); // Get tasks by userId & todoId
    this.router.get('/:userId/todos/:todoId/tasks/:taskId', userController.getTask);
    this.router.put('/:userId/todos/:todoId/tasks/:taskId', userController.updateTask);
    this.router.delete('/:userId/todos/:todoId/tasks/:taskId', userController.deleteTask);
  }
}

const userRoutes = new User();
export default userRoutes.router;
